package com.vnshine.test;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vnshine.lib.VnshineController;

/**
 * Created by hungnguyenvan on 1/4/18.
 */

public class MainActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VnshineController.getInstance().active(getBaseContext());
    }
}
