package com.vnshine.lib;

import android.content.Context;

import com.vnshine.lib.retrofit.ApiUtils;
import com.vnshine.lib.retrofit.AppId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hungnguyenvan on 1/4/18.
 */

public class VnshineLibUtils  {

    public static VnshineLibUtils instance = new VnshineLibUtils();


    public static VnshineLibUtils getInstance(){
        return instance;
    }



    public void active(Context baseContext) {
        PreferenceManager preferenceManager = new PreferenceManager();
        preferenceManager.init(baseContext);
        String packageName = baseContext.getPackageName();
        if (NetworkUtils.isNetworkConnected(baseContext)) {
            ApiUtils.getAppIdApi().getId(packageName).enqueue(new Callback<AppId>() {
                @Override
                public void onResponse(Call<AppId> call, Response<AppId> response) {
                    if (response.body().getIsupdate()) {
                        PreferenceManager.getInstance().save(PreferenceManager.BANNERID,response.body().getIdquangcaobaner(),String.class);
                        PreferenceManager.getInstance().save(PreferenceManager.FULLID,response.body().getIdquangcaofull(),String.class);
                        PreferenceManager.getInstance().save(PreferenceManager.ID_APP,response.body().getIdapp(),String.class);

                    }
                }

                @Override
                public void onFailure(Call<AppId> call, Throwable t) {

                }
            });
        }
    }
}
