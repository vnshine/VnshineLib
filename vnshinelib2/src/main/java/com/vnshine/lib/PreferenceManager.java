package com.vnshine.lib;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hungnguyenvan on 1/4/18.
 */

public class PreferenceManager{


    public static final String FULLID ="vnshine_fullid";
    public static final String BANNERID = "vnshine_bannerid";
    public static final String ID_APP = "vnshine_id";
    public static final String UPDATE = "vnshine_update";
    public static final String  ANALYTIC_ID = "vnshine_analytic";
    public Context mainContext;
    public static PreferenceManager instance = new PreferenceManager();
    public SharedPreferences mPreference;

    public void init(Context context){
        mPreference = mainContext.getSharedPreferences(mainContext.getPackageName(),Context.MODE_PRIVATE);
    }
    public PreferenceManager(){

    }
    public static PreferenceManager getInstance(){

        return instance;
    }




    public void save(String key, Object value, Class<?> valueCls){
        if (valueCls == String.class) {

            saveString(key, (String) value);
        } else if (valueCls == Boolean.class) {

            saveBoolean(key, (Boolean) value);
        }
    }

    private void saveString(String key, String value) {
        SharedPreferences.Editor editor = mPreference.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mPreference.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public <T> T get(String key, Class<T> valueCls) {

        T result = null;
        if (valueCls == String.class) {

            result = (T) getString(key);
        } else if (valueCls == Boolean.class) {

            result = (T) getBoolean(key);
        }
        return result;
    }
    private String getString(String key) {

        if(mPreference.contains(key)) {

            return mPreference.getString(key,"");
        } else {

            return null;
        }
    }

    private Boolean getBoolean(String key) {

        if (mPreference.contains(key)) {

            return mPreference.getBoolean(key,false);
        } else {

            return true;
        }
    }
}