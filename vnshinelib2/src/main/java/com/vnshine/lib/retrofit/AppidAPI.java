package com.vnshine.lib.retrofit;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Canh7antt8a on 15/12/2017.
 */

public interface AppidAPI {
    @GET("api.php")
    Call<AppId> getId(@Query("id") String id);
}
