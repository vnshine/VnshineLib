
package com.vnshine.lib.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppId implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id="1";
    @SerializedName("namequangcao")
    @Expose
    private String namequangcao="ApplockAndroid";
    @SerializedName("idquangcaofull")
    @Expose
    private String idquangcaofull="ca-app-pub-7888391699647777/5430416195";
    @SerializedName("idquangcaobaner")
    @Expose
    private String idquangcaobaner="ca-app-pub-7888391699647777/7303391750";
    @SerializedName("idapp")
    @Expose
    private String idapp="ca-app-pub-7888391699647777~4308906213";
    @SerializedName("isupdate")
    @Expose
    private boolean isupdate=true;
    private final static long serialVersionUID = -8225988974837395187L;

    /**
     * No args constructor for use in serialization
     *
     */
    public AppId() {
    }

    /**
     *
     * @param id
     * @param idquangcaofull
     * @param idquangcaobaner
     * @param idapp
     * @param isupdate
     * @param namequangcao
     */
    public AppId(String id, String namequangcao, String idquangcaofull, String idquangcaobaner, String idapp, boolean isupdate) {
        super();
        this.id = id;
        this.namequangcao = namequangcao;
        this.idquangcaofull = idquangcaofull;
        this.idquangcaobaner = idquangcaobaner;
        this.idapp = idapp;
        this.isupdate = isupdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamequangcao() {
        return namequangcao;
    }

    public void setNamequangcao(String namequangcao) {
        this.namequangcao = namequangcao;
    }

    public String getIdquangcaofull() {
        return idquangcaofull;
    }

    public void setIdquangcaofull(String idquangcaofull) {
        this.idquangcaofull = idquangcaofull;
    }

    public String getIdquangcaobaner() {
        return idquangcaobaner;
    }

    public void setIdquangcaobaner(String idquangcaobaner) {
        this.idquangcaobaner = idquangcaobaner;
    }

    public String getIdapp() {
        return idapp;
    }

    public void setIdapp(String idapp) {
        this.idapp = idapp;
    }

    public boolean getIsupdate() {
        return isupdate;
    }

    public void setIsupdate(boolean isupdate) {
        this.isupdate = isupdate;
    }

}
