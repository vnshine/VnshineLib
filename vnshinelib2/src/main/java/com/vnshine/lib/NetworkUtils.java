package com.vnshine.lib;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by hungnguyenvan on 1/4/18.
 */

public class NetworkUtils{
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
